const Product = require('../models/product')
const Cart = require('../models/cart')


exports.getProducts = (req, res,next) => {
    Product.fetchAll()
    .then(products => {
        res.render('shop/product-list',{
            pageTitle: 'Home',
            prods: products
        })
    })
    .catch(err=> {
        console.log(err)
    })
      
    
}

exports.getProduct = (req,res,next)=> {
    const prodId = req.params.productId;
    Product.findById(prodId)
    .then(product => {
        res.render('shop/product-detail',{
            pageTitle: 'Home',
             product
        })
    })
    .catch(err=> {
        console.log(err)
    })
    
}

exports.getIndex = (req,res,next)=> {
    Product.fetchAll()
    .then(products => {
        res.render('shop/product-list',{
            pageTitle: 'Home',
            prods: products
        })
    })
    .catch(err=> {
        console.log(err)
    })
}

exports.getOrders = (req,res,next)=> {
    Product.fetchAll(products => {
        res.render('shop/orders',{
            pageTitle: 'Your Orders',
            prods: products
        })
    })
}

exports.getCart = (req,res,next)=> {
    Cart.getCart(cart => {
      Product.fetchAll(products => {
          let cartProducts = []
          for(product of products){
              const cartProductData = cart.products.find(prod => prod.id === product.id)
              if(cartProductData) {
                cartProducts.push({productData: product,qty: cartProductData.qty})
              }
          }
          res.render('shop/cart', {
              pageTitle:'Your cart',
              products: cartProducts
          })
      })
    })
 
}
exports.postCart = (req,res,next)=> {
    const prodId = req.body.productId;
    Product.findById(prodId,product => {
        Cart.addProduct(prodId,product.price)
    })
    res.redirect('/')
}
exports.getCheckout = (req,res,next)=> {
    res.render('shop/checkout',{
        pageTitle: 'Checkout '
    })
}

exports.postDeleteCart = (req,res,next)=> {
    const prodId = req.body.productId;
    Product.findById(prodId, product=> {
        Cart.deleteProduct(prodId,product.price);
        res.redirect('/cart')
    })
}