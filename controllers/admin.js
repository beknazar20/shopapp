const Product = require('../models/product')
const mongodb = require('mongodb')

const ObjectId = mongodb.ObjectId;
exports.getAddProduct = (req, res) => {
    res.render('admin/edit-product', {
        pageTitle: 'Add Product',
        editing: false
    });
}


exports.getEditProduct = (req, res) => {
    const editMode = req.query.edit;
    if (!editMode) {
        return res.redirect('/')
    }
    const prodId = req.params.productId
    Product.findById(prodId)
    .then(product => {
        res.render('admin/edit-product', {
            pageTitle: 'Edit Product',
            editing: editMode,
            product
        });
    })
    .catch(err => {
        console.log(err)
        res.redirect('/')
    })
   
 

}

exports.postAddProduct = (req, res) => {
        title = req.body.title,
        imgURL = req.body.imgURL,
        price = req.body.price,
        description = req.body.description
    const product = new Product(
        title, 
        imgURL,
        price,
        description,
        null,
        req.user._id)
    product.save()
        .then( res.redirect('/admin/products'))
}

exports.getProducts = (req, res, next) => {
    Product.fetchAll()
    .then(products => {
        console.log(req.user)
        res.render('admin/products',{
            pageTitle: 'Your products',
            prods: products
        })
    })
    .catch(err=> {
        console.log(err)
    })
}

exports.postEditProduct = (req, res, next) => {
    const prodId = req.body.productId;
    const updatedTitle = req.body.title;
    const updatedPrice = req.body.price;
    const updatedImageUrl = req.body.imgURL;
    const updatedDesc = req.body.description;
                const product = new Product(
                updatedTitle,
                updatedImageUrl, 
                updatedPrice,
                updatedDesc,
                new ObjectId(prodId),
            );
             product.save()
            .then(result => {
                console.log('UPDATED PRODUCT: ' , result)
                res.redirect('/admin/products')
            })
        .catch(err => {
            console.log('No products',err)
        })
}

exports.postDeleteProduct = (req,res,next) => {
    const prodId = req.body.productId
    Product.deleteById(prodId)
    .then(()=> {
        console.log('Product Deleted')
            res.redirect('/admin/products')

    })
    .catch(err => {
        console.log(err)
    });
}