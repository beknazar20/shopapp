const path = require('path')
const express = require('express')
const router = express.Router();
const rootDir = require('../util/path')
// const adminData = require('./admin')
const shopController = require('../controllers/shop')
const app = express()
// var bodyParser = require('body-parser');
// app.use(bodyParser.urlencoded({ extended: false }))
// app.use(bodyParser.json())


router.get('/', shopController.getIndex)
router.get('/products', shopController.getProducts)
router.get('/products/:productId', shopController.getProduct)
// router.get('/cart', shopController.getCart)
// router.post('/cart', shopController.postCart)
// router.post('/cart-delete-item', shopController.postDeleteCart)
// router.get('/orders', shopController.getOrders)
// router.get('/chekout', shopController.getCheckout)

module.exports = router