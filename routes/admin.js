const express = require('express')
const app = express()
const router = express.Router();
const path = require('path')
const adminController = require('../controllers/admin')
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

router.get('/add-product',adminController.getAddProduct);
 router.get('/products',adminController.getProducts);
 
router.post('/add-product', adminController.postAddProduct )

router.get('/edit-product/:productId', adminController.getEditProduct )

 router.post('/edit-product', adminController.postEditProduct)

 router.post('/delete-product', adminController.postDeleteProduct)

module.exports = router;
//  exports.products = products;