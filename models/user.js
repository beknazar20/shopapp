const getDb = require('../util/database').getDb 
 const mongodb = require('mongodb')

 const ObjectId = mongodb.ObjectId
 class User {
     constructor(username, email){
         this.username = username,
         this.email = email
     }
     save(){
       const db = getDb();
       return db.collection('myusers').insertOne(this);
     }
     static findById(userId) {
        const db = getDb();
        return db
          .collection('myusers')
          .findOne({ _id: new ObjectId(userId) })
          .then(user => {
            console.log(user);
            return user;
          })
          .catch(err => {
            console.log(err);
          });
      }
    //  static findById(userId){
    //     const db = getDb();
    //     return db.collection('myusers')
    //     .findOne({_id: new ObjectId(userId)})
    //     .then(user => {
    //         console.log(user)
    //         return user;
    //     })
    //     .catch(err => {
    //         console.log(err)
    //     })

    //  }
 }
 
 module.exports = User