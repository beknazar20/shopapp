const express = require('express')
const app = express();
const path = require('path')
const adminRoute = require('./routes/admin')
const shopRoute = require('./routes/shop')
const errorController = require('./controllers/error')
const mongoConnect = require('./util/database').mongoConnect;
app.use(express.static(path.join(__dirname, 'public')))
const db = require('./util/database')
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }))

const User = require('./models/user')

app.use((req, res, next) => {
    User.findById('5f65db1b7db594918d4a9d2e')
      .then(user => {
        req.user = user;
        next();
      })
      .catch(err => console.log(err));
  });
// function createUser(){
//     const user = new User (
//        'Beknazar',
//        'test@mail.ru'
//     )
    
//     user.save()
// }



// parse application/json

app.use(bodyParser.json())
app.set('view engine', 'ejs')
app.set('views', 'views')


//Routes
app.use('/admin',adminRoute)
app.use(shopRoute)

app.use(errorController.get404)



  


mongoConnect(() => {
    app.listen(3000, ()=> {
        console.log('Server started on ports 3000')
    })
})





