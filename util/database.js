const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
let _db;
const mongoConnect = callback => {
    MongoClient.connect('mongodb+srv://Beknazar:O38LM5bKyxDF05S2@shop-kuqtq.mongodb.net/shopApp',
    { useUnifiedTopology: true })
        .then(client => {
            _db = client.db('shopApp')
            callback(client)

        })
        .catch(err => {
            callback(err)
        })
}

const getDb = () => {
    if (_db) {
        return _db;
    }
    throw 'NO DATABASE FOUND'
}

exports.mongoConnect = mongoConnect;
exports.getDb = getDb;